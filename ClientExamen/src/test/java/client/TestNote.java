package client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import persistance.ClassRoom;
import persistance.Module;
import persistance.Student;
import service.GestionNoteServiceRemote;

public class TestNote {
	GestionNoteServiceRemote remote;

	@Before
	public void init() {
		try {
			Context context = new InitialContext();

			remote = (GestionNoteServiceRemote) context
					.lookup("/note/GestionNoteService!service.GestionNoteServiceRemote");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Ajouter des modules
	 */
	@Test
	public void _1doAjoterModule() {
		Module jee = new Module("Architecture n tiers JEE", 42, "Examen");
		Assert.assertEquals(true, remote.addModule(jee));
		Module spring = new Module("Spring", 21, "TP");
		Assert.assertEquals(true, remote.addModule(spring));
		Module java = new Module("Java", 42, "DS");
		Assert.assertEquals(true, remote.addModule(java));
		Module altelier = new Module("Atelier JEE", 21, "TP");
		Assert.assertEquals(true, remote.addModule(altelier));
	}

	/**
	 * 
	 * Ajouter des class
	 */
	@Test
	public void _2doAjouterClassRoom() {
		ClassRoom erp1 = new ClassRoom();
		erp1.setName("4ERP-BI1");
		Assert.assertEquals(true, remote.addClassRoom(erp1));

		ClassRoom erp2 = new ClassRoom();
		erp2.setName("4ERP-BI2");
		Assert.assertEquals(true, remote.addClassRoom(erp2));

		ClassRoom erp3 = new ClassRoom();
		erp3.setName("4ERP-BI3");
		Assert.assertEquals(true, remote.addClassRoom(erp3));

		ClassRoom erp4 = new ClassRoom();
		erp4.setName("4ERP-BI4");
		Assert.assertEquals(true, remote.addClassRoom(erp4));
	}

	/**
	 * 
	 * 
	 * Affecter des etudiants aux class
	 */
	@Test
	public void _3doAffecterEtudiantToClassRoom() {
		ClassRoom erp1 = remote.findClassRoomById(1);
		List<Student> students = new ArrayList<>();
		Student student = new Student("Ahmed", "Ben Ahmed");
		Student student1 = new Student("Mouhamed", "Ben mouhamed");
		students.add(student);
		students.add(student1);
		Assert.assertEquals(true,
				remote.affectStudentsToClassRoom(erp1, students));
		ClassRoom erp2 = remote.findClassRoomById(2);
		List<Student> students1 = new ArrayList<>();
		Student student3 = new Student("Foulen", "Ben Foulen");
		Student student4 = new Student("Emna", "Ben Mohamed");
		students1.add(student3);
		students1.add(student4);

		Assert.assertEquals(true,
				remote.affectStudentsToClassRoom(erp2, students1));

	}

	/**
	 * 
	 * Ajouter des notes pour des etudiants par module
	 */
	@Test
	public void _4doAjouterNoteToEtudiants() {
		Assert.assertEquals(true, remote.addnote(13, new Date(), "Spring", 1));
		Assert.assertEquals(true, remote.addnote(17, new Date(), "Spring", 2));
		Assert.assertEquals(true, remote.addnote(10, new Date(), "Java", 1));
		Assert.assertEquals(true,
				remote.addnote(11, new Date(), "Atelier JEE", 2));
		Assert.assertEquals(true,
				remote.addnote(07, new Date(), "Atelier JEE", 1));

	}

	/**
	 * List of module by classRomm when exam passed
	 */
	@Test
	public void _5listDesModulesbyExamPassed() {
		Assert.assertEquals(3, remote.findAllModuleByClass("4ERP-BI1").size());

	}
}
